#include <stdio.h>
#include <stdlib.h>

#define N1 3
#define N2 3
#define N 6

void printpoly3(int nx, int ny, int nz, int*** p);

int*** multpoly3(
         int nx,  int ny,  int nz,  int ***dest,
         int nx1, int ny1, int nz1, int ***p1,
         int nx2, int ny2, int nz2, int ***p2);

int main(void)
{
    int poly1[N1][N1][N1];
    int poly2[N2][N2][N2];
    int poly[N][N][N] = {}; // 모두 0으로 초기화

    for (int i=0; i<N1; ++i)
        for (int j=0; j<N1; ++j)
            for (int k=0; k<N1; ++k)
                scanf("%d", &(poly1[i][j][k])); // x^iy^jz^k의 계수 입력

    for (int i=0; i<N2; ++i)
        for (int j=0; j<N2; ++j)
            for (int k=0; k<N2; ++k)
                scanf("%d", *(*(poly2+i)+j)+k); // x^iy^jz^k의 계수 입력

    int ***p1 = malloc(N1*sizeof(int**));
    for(int i = 0; i < N1; ++i)
    {
        p1[i] = malloc(N1*sizeof(int*));
        for(int j = 0; j < N1; ++j)
        {
            p1[i][j] = malloc(N1*sizeof(int));
        }
    }
    for (int i=0; i<N1; ++i)
    for (int j=0; j<N1; ++j)
    for (int k=0; k<N1; ++k)
    p1[i][j][k] = poly1[i][j][k];

    int ***p2 = malloc(N2*sizeof(int**));
    for(int i = 0; i < N2; ++i)
    {
        p2[i] = malloc(N2*sizeof(int*));
        for(int j = 0; j < N2; ++j)
        {
            p2[i][j] = malloc(N2*sizeof(int));
        }
    }
    for (int i=0; i<N2; ++i)
    for (int j=0; j<N2; ++j)
    for (int k=0; k<N2; ++k)
    p2[i][j][k] = poly2[i][j][k];

    int ***p = malloc(N*sizeof(int**));
    for(int i = 0; i < N; ++i)
    {
        p[i] = malloc(N*sizeof(int*));
        for(int j = 0; j < N; ++j)
        {
            p[i][j] = malloc(N*sizeof(int));
        }
    }
    for (int i=0; i<N; ++i)
    for (int j=0; j<N; ++j)
    for (int k=0; k<N; ++k)
    p[i][j][k] = poly[i][j][k];

    multpoly3(N,N,N,p, N1,N1,N1,p1, N2,N2,N2,p2);

    printf("  "); printpoly3(N1,N1,N1,p1); printf("\n");
    printf("* "); printpoly3(N2,N2,N2,p2); printf("\n");
    printf("------------------------------------------------------------\n");
    printf("  "); printpoly3(N,N,N,p); printf("\n");

    for(int i = 0; i < N1; ++i)
    {
        for(int j = 0; j < N1; ++j)
        {
            free(p1[i][j]);
        }
        free(p1[i]);
    }
    free(p1);
    for(int i = 0; i < N2; ++i)
    {
        for(int j = 0; j < N2; ++j)
        {
            free(p2[i][j]);
        }
        free(p2[i]);
    }
    free(p2);
    for(int i = 0; i < N; ++i)
    {
        for(int j = 0; j < N; ++j)
        {
            free(p[i][j]);
        }
        free(p[i]);
    }
    free(p);
    return 0;
}

void printpoly3(int nx, int ny, int nz, int*** p)
{
    int a = 0;
    for(int i = 0; i < nx; ++i)
    for(int j = 0; j < ny; ++j)
    for(int k = 0; k < nz; ++k)
    {
        if(a == 0)
        {
            if(p[i][j][k] == 0)
            {
                printf("\0");
            }else
            {
                if(p[i][j][k] == 1)
                {
                    if(i == 0 && j == 0 && k == 0)
                    {
                        printf("1");
                    }
                    if(i == 1)
                    {
                        printf("x");
                    }else if(i != 0)
                    {
                        printf("x^%d", i);
                    }
                    if(j == 1)
                    {
                        printf("y");
                    }else if(j != 0)
                    {
                        printf("y^%d", j);
                    }
                    if(k == 1)
                    {
                        printf("z");
                    }else if(k != 0)
                    {
                        printf("z^%d", k);
                    }
                }else if(p[i][j][k] != 1 && p[i][j][k] > 0)
                {
                    printf("%d", p[i][j][k]);
                    if(i == 1)
                    {
                        printf("x");
                    }else if(i != 0)
                    {
                        printf("x^%d", i);
                    }
                    if(j == 1)
                    {
                        printf("y");
                    }else if(j != 0)
                    {
                        printf("y^%d", j);
                    }
                    if(k == 1)
                    {
                        printf("z");
                    }else if(k != 0)
                    {
                        printf("z^%d", k);
                    }
                }else if(p[i][j][k] == -1)
                {
                    if(i == 0 && j == 0 && k == 0)
                    {
                        printf("-1");
                    }else
                    {
                        printf("-");
                    }
                    if(i == 1)
                    {
                        printf("x");
                    }else if(i != 0)
                    {
                        printf("x^%d", i);
                    }
                    if(j == 1)
                    {
                        printf("y");
                    }else if(j != 0)
                    {
                        printf("y^%d", j);
                    }
                    if(k == 1)
                    {
                        printf("z");
                    }else if(k != 0)
                    {
                        printf("z^%d", k);
                    }
                }else
                {
                    printf("%d", p[i][j][k]);
                    if(i == 1)
                    {
                        printf("x");
                    }else if(i != 0)
                    {
                        printf("x^%d", i);
                    }
                    if(j == 1)
                    {
                        printf("y");
                    }else if(j != 0)
                    {
                        printf("y^%d", j);
                    }
                    if(k == 1)
                    {
                        printf("z");
                    }else if(k != 0)
                    {
                        printf("z^%d", k);
                    }
                }
                a = 1;
            }
        }else
        {
            if(p[i][j][k] == 0)
            {
                printf("\0");
            }else
            {
                if(p[i][j][k] == 1)
                {
                    if(i == 0 && j == 0 && k == 0)
                    {
                        printf(" +1");
                    }else
                    {
                        printf(" +");
                    }
                    if(i == 1)
                    {
                        printf("x");
                    }else if(i != 0)
                    {
                        printf("x^%d", i);
                    }
                    if(j == 1)
                    {
                        printf("y");
                    }else if(j != 0)
                    {
                        printf("y^%d", j);
                    }
                    if(k == 1)
                    {
                        printf("z");
                    }else if(k != 0)
                    {
                        printf("z^%d", k);
                    }
                }else if(p[i][j][k] != 1 && p[i][j][k] > 0)
                {
                    printf(" +%d", p[i][j][k]);
                    if(i == 1)
                    {
                        printf("x");
                    }else if(i != 0)
                    {
                        printf("x^%d", i);
                    }
                    if(j == 1)
                    {
                        printf("y");
                    }else if(j != 0)
                    {
                        printf("y^%d", j);
                    }
                    if(k == 1)
                    {
                        printf("z");
                    }else if(k != 0)
                    {
                        printf("z^%d", k);
                    }
                }else if(p[i][j][k] == -1)
                {
                    if(i == 0 && j == 0 && k == 0)
                    {
                        printf(" -1");
                    }else
                    {
                        printf(" -");
                    }
                    if(i == 1)
                    {
                        printf("x");
                    }else if(i != 0)
                    {
                        printf("x^%d", i);
                    }
                    if(j == 1)
                    {
                        printf("y");
                    }else if(j != 0)
                    {
                        printf("y^%d", j);
                    }
                    if(k == 1)
                    {
                        printf("z");
                    }else if(k != 0)
                    {
                        printf("z^%d", k);
                    }
                }else
                {
                    printf(" %d", p[i][j][k]);
                    if(i == 1)
                    {
                        printf("x");
                    }else if(i != 0)
                    {
                        printf("x^%d", i);
                    }
                    if(j == 1)
                    {
                        printf("y");
                    }else if(j != 0)
                    {
                        printf("y^%d", j);
                    }
                    if(k == 1)
                    {
                        printf("z");
                    }else if(k != 0)
                    {
                        printf("z^%d", k);
                    }
                }
            }
        }
    }
    if(a == 0)
    {
        printf("0");
    }
}
int*** multpoly3(
         int nx,  int ny,  int nz,  int ***dest,
         int nx1, int ny1, int nz1, int ***p1,
         int nx2, int ny2, int nz2, int ***p2)
{
    for(int a = 0; a < nx1; ++a)
    for(int b = 0; b < ny1; ++b)
    for(int c = 0; c < nz1; ++c)
    for(int d = 0; d < nx2; ++d)
    for(int e = 0; e < ny2; ++e)
    for(int f = 0; f < nz2; ++f)
    for(int g = 0; g < nx; ++g)
    for(int h = 0; h < ny; ++h)
    for(int i = 0; i < nz; ++i)
    {
        if(g == a + d && h == b + e && i == c + f)
        {
            dest[g][h][i] += p1[a][b][c] * p2[d][e][f];
        }
    }
    return dest;
}
