#include <stdio.h>
#include <stdlib.h>

void triR(void) {
  int size, repeat;
  int h, i, j;
	scanf("%d %d", &size, &repeat);
	printf("Hello world\n");
	for(h = 1; h <= repeat; ++h){
    printf("\n");
	for(i = 1; i <= size; ++i){
    for(j = 1; j < i + 1; ++j)
    {
    printf("%d", i);
    }
    printf("\n");
    }
	for(i = size; i > 1; --i){
    for(j = 1; j < i; ++j){
    printf("%d", i - 1);
    }
    printf("\n");
    }

	}
	printf("Bye world\n");
}

void triL(void) {
  int size, repeat;
  int h, i, j;
	scanf("%d %d", &size, &repeat);
	printf("Hello world\n");
	for(h = 1; h <= repeat; ++h){
    for(i = 1; i <= size; ++i){
        printf(".");
    }
    printf("\n");
    for(i = 1; i <= size; ++i){
        for(j = size + 1 - i; j > 0; --j){
            if(j == 1){
                printf("");
            }else{printf(".");}
        }
        for(j = 1; j <= i; ++j){
            printf("%d", i);
        }
        printf("\n");
    }
    for(i = size - 1; i >= 1; --i){
        for(j = 1; j <= size - i; ++j){
            printf(".");
        }
        for(j = 1; j <= i; ++j){
            printf("%d", i);
        }
        printf("\n");
    }
    }
    printf("Bye world\n");
    }

void dias(void) {
  int size, repeat;
  int h, i, j;
	scanf("%d %d", &size, &repeat);
	printf("Hello world\n");
	for(h = 1; h <= repeat; ++h){
    for(i = 1; i <= size; ++i){
        printf(".");
    }
    printf("\n");
    for(i = 1; i <= size; ++i){
        for(j = size + 1 - i; j > 0; --j){
            if(j == 1){
                printf("");
            }else{printf(".");}
        }
        for(j = 1; j <= i; ++j){
            printf("%d%d", i, i);
        }
        printf("\n");
    }
    for(i = size - 1; i >= 1; --i){
        for(j = 1; j <= size - i; ++j){
            printf(".");
        }
        for(j = 1; j <= i; ++j){
            printf("%d%d", i, i);
        }
        printf("\n");
    }
    }
    printf("Bye world\n");
}

int main(void)
{
  int n;
  scanf("%d", &n); // 1,2,3 중 하나를 입력받는다
  switch (n)
  {
    case 1: triR(); break;
    case 2: triL(); break;
    case 3: dias(); break;
    default: return -1;
  }
  return 0;
}
