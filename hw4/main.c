#include <stdio.h>

#define N1 10
#define N2 10
#define N  20

void printpoly(int, const int []);

int* multpoly(int dest[],
              int n1, const int poly1[],
              int n2, const int poly2[]);

int main()
{
    int poly1[N1];
    int poly2[N2];
    int poly[N] = {}; // 모두 0으로 초기화

    for (int i=0; i<N1; ++i) scanf("%d", &poly1[i]); // poly1 입력 받기
    for (int i=0; i<N2; ++i) scanf("%d", poly2 + i); // poly2 입력 받기
    multpoly(poly, N1, poly1, N2, poly2); // poly에 poly1과 poly2의 곱을 저장

    printf("  "); printpoly(N1, poly1); printf("\n");
    printf("* "); printpoly(N2, poly2); printf("\n");
    printf("------------------------------------------------------------\n");
    printf("  "); printpoly(N, poly); printf("\n");

    return 0;
}


void printpoly(int n, const int poly[])
{
    int j = 1;
    for(int i = 0; i < n; i++)
    {
        if(j == 1)
        {
            if(poly[i] != 0)
            {
                if(0 < poly[i] && 1 != poly[i])
                {
                    if(i == 0)
                    {
                        printf("%d", poly[i]);
                    }else if(i == 1)
                    {
                        printf("%d", poly[i]);
                        printf("x");
                    }else
                    {
                        printf("%d", poly[i]);
                        printf("x^%d", i);
                    }
                }else if(poly[i] == 1)
                {
                    if(i == 0)
                    {
                        printf("%d", poly[i]);
                    }else if(i == 1)
                    {
                        printf("x");
                    }else
                    {
                        printf("x^%d", i);
                    }
                }
                else if(poly[i] < 0 && poly[i] != -1)
                {
                    if(i == 0)
                    {
                        printf("%d", poly[i]);
                    }else if(i == 1)
                    {
                        printf("%d", poly[i]);
                        printf("x");
                    }else
                    {
                        printf("%d", poly[i]);
                        printf("x^%d", i);
                    }
                }else
                {
                    if(i == 0)
                    {
                        printf("%d", poly[i]);
                    }else if(i == 1)
                    {
                        printf("-x");
                    }else
                    {
                        printf("-x^%d", i);
                    }
                }
                j = 2;
            }else
            {
                printf("\0");
            }
        }else
        {
            if(poly[i] != 0)
            {
                if(0 < poly[i] && 1 != poly[i])
                {
                    if(i == 1)
                    {
                        printf(" +%d", poly[i]);
                        printf("x");
                    }else
                    {
                        printf(" +%d", poly[i]);
                        printf("x^%d", i);
                    }
                }else if(poly[i] == 1)
                {
                    if(i == 1)
                    {
                        printf(" +x");
                    }else
                    {
                        printf(" +x^%d", i);
                    }
                }
                else if(poly[i] < 0 && poly[i] != -1)
                {
                    if(i == 1)
                    {
                        printf(" %d", poly[i]);
                        printf("x");
                    }else
                    {
                        printf(" %d", poly[i]);
                        printf("x^%d", i);
                    }
                }else
                {
                    if(i == 1)
                    {
                        printf(" -x");
                    }else
                    {
                        printf(" -x^%d", i);
                    }
                }
            }else
            {
                printf("\0");
            }
        }
    }
    if(j == 1)
    {
        printf("0");
    }
}

int* multpoly(int dest[],
              int n1, const int poly1[],
              int n2, const int poly2[])
{
    for(int i=0; i<n1; ++i)
    {
        for(int j=0; j<n2; ++j)
        {
            for(int h=0; h<n1+n2; ++h)
            {
                if(h == i+j)
                {
                    dest[h]+=(poly1[i]*poly2[j]);
                }
            }
        }
    }

    return dest;
}
